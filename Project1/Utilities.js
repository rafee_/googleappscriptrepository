function resetProcessed() {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheets()[1];

  var lastrow= sheet.getLastRow();
  for(var i=1;i<=lastrow;i++){
    var processed=sheet.getRange(i,3).getValue();
    if(processed.toString().toLowerCase()=="yes"){
      var cell=sheet.getRange(i,3);
      cell.setValue("no");
    }
  }
}

function resetOutput(){
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheets()[0];
  
  var lastRow=sheet.getLastRow();
  var lastCol=3;
  if(lastRow>0){ 
    var rannge=sheet.getRange(2,2,lastRow,lastCol);
    rannge.clear();
  }

}

function createRange(){
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheets()[1];
  
  var lastRow=sheet.getLastRow();
  var lastCol=3;
  if(lastRow>0){ 
    var rannge=sheet.getRange(1,1,lastRow,lastCol);
    rannge.clear();
  }
  

  var start=2;
  var stop=50001;
  var diff=getDifference();

  var rowNum=1;

  for(var i=start;i<=stop;i=i+diff){

    var cell3=sheet.getRange(rowNum,3);
    cell3.setValue("no");
    var cell1=sheet.getRange(rowNum,1);
    cell1.setValue(i);
    var cell2=sheet.getRange(rowNum,2);
    cell2.setValue(i+diff-1);
    rowNum++;
  }
}

function getDifference(){
  var ui=SpreadsheetApp.getUi();
  var result=ui.prompt('Create Range','Select the difference',ui.ButtonSet.OK);
  var button=result.getSelectedButton();
  if(button==ui.Button.OK){
    var text=result.getResponseText();
    return parseInt(text);
  }
  return 50;
}