function onOpen(e){
  var ui=SpreadsheetApp.getUi();

  ui.createMenu('Custom Menu')
  .addItem('Check Email Validity','checkEmailValidity')
  .addSeparator()
  .addItem('Reset Processed Status','resetProcessed')
  .addSeparator()
  .addItem('Reset Output','resetOutput')
  .addSeparator()
  .addItem('Create New Ranges','createRange')
  .addToUi()
}