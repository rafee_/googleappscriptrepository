function run(){
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheets()[1];

  var lastrow= sheet.getLastRow();
  for(var i=1;i<=lastrow;i++){
    var processed=sheet.getRange(i,3).getValue();
    if(processed.toString().toLowerCase()=="no"){
      var cell=sheet.getRange(i,3);
      cell.setValue("yes");

      var start = sheet.getRange(i,1).getValue();
      var stop = sheet.getRange(i,2).getValue();
      checkEmailValidityInRange(start,stop);
      break;
    }
  }
}