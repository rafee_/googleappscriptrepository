# README #

This repo is for the collection of Google App Script codes 
that were used by me in various projects.

### Quick Points to be Noted ###

* These are .gs files but they are saved as .js file for formatting support
* Project contains some private APIs, so these should be carefully managed.
* [Contact Me](https://www.twitter.com/dfrtwo)
